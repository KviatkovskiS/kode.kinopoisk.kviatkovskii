//
//  KSCollectionReusableView.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 28.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSCollectionReusableView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *titleHeaderCell;
@property (weak, nonatomic) IBOutlet UILabel *addressHeaderCell;
@property (weak, nonatomic) IBOutlet UIButton *btnHeaderCell;

@end
