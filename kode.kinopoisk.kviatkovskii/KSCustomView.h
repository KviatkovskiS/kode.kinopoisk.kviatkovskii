//
//  KSCustomView.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 20.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSObject+KSColor_hex.h"

@interface KSCustomView : UIView

@end
