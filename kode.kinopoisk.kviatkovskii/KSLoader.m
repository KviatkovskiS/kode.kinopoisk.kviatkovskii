//
//  KSLoader.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 02.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSLoader.h"

@implementation KSLoader

+ (void)downloadFilmByCity:(NSString *)cityID filmDate:(NSString *)date filmSort:(NSInteger)parameter genreFilter:(NSString *)genreName block:(void(^)(NSDictionary *completion))completion {

    __block NSString *strDate;
    
    NSString *urlCity = [NSString stringWithFormat:@"http://api.kinopoisk.cf/getTodayFilms?date=%@&cityID=%@", date, cityID];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:urlCity parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"%@", responseObject);
        @autoreleasepool {
            NSMutableDictionary *dictGenre = [NSMutableDictionary dictionary];
            NSMutableDictionary *dictTitle = [NSMutableDictionary dictionary];
            NSMutableDictionary *dictCountry = [NSMutableDictionary dictionary];
            NSMutableDictionary *dictRaiting = [NSMutableDictionary dictionary];
            NSMutableDictionary *dictPoster = [NSMutableDictionary dictionary];
            NSMutableDictionary *dictID = [NSMutableDictionary dictionary];
            __block id isEmpty;
            
           @try {
                strDate = [KSDate convertYandexDateToStringDate:responseObject[@"date"]];
                [responseObject[@"filmsData"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    if ([genreName isEqualToString:@"ALL"]) {
                        [dictGenre setValue:obj[@"genre"] forKey:obj[@"id"]];
                        [dictTitle setValue:obj[@"nameRU"] forKey:obj[@"id"]];
                        [dictCountry setValue:obj[@"country"] forKey:obj[@"id"]];
                        if (obj[@"ratingVoteCount"]) {
                            [dictRaiting setValue:[NSString stringWithFormat:@"%@ (%@)", obj[@"rating"] ? obj[@"rating"]:@"0.00", obj[@"ratingVoteCount"]]
                                           forKey:obj[@"id"]];
                        } else {
                            [dictRaiting setValue:obj[@"rating"] ? obj[@"rating"]:@"0.00" forKey:obj[@"id"]];
                        }
                        [dictPoster setValue:[NSString stringWithFormat:@"https://st.kp.yandex.net/images/film_big/%@.jpg", obj[@"id"]] forKey:obj[@"id"]];
                        [dictID setValue:[KSSort sortDictionary:dictRaiting sortParameter:parameter] forKey:strDate];
                    }
                    else {
                        if ([KSFilter containsArray:[genreName componentsSeparatedByString:@","] genreString:obj[@"genre"]]) {
                            [dictGenre setValue:obj[@"genre"] forKey:obj[@"id"]];
                            [dictTitle setValue:obj[@"nameRU"] forKey:obj[@"id"]];
                            [dictCountry setValue:obj[@"country"] forKey:obj[@"id"]];
                            [dictRaiting setValue:obj[@"rating"] ? obj[@"rating"]:@"0.00" forKey:obj[@"id"]];
                            [dictPoster setValue:[NSString stringWithFormat:@"https://st.kp.yandex.net/images/film_big/%@.jpg", obj[@"id"]] forKey:obj[@"id"]];
                            [dictID setValue:[KSSort sortDictionary:dictRaiting sortParameter:parameter] forKey:strDate];
                        }
                    }
                }];

               if (responseObject[@"filmsData"]) {
                   isEmpty = dictID.count != 0 ? @0:@1;
               } else {
                   isEmpty = @1;
               }
            } @catch (NSException *exception) {
                NSLog(@"%@", exception.description);
                isEmpty = @1;
            } @finally {
                completion(@{@"isEmpty": isEmpty,
                             @"Genre": dictGenre,
                             @"Title": dictTitle,
                             @"Country": dictCountry,
                             @"Raiting": dictRaiting,
                             @"Poster": dictPoster,
                             @"ID": dictID,
                             @"Date": strDate ? strDate:@""});
                [SVProgressHUD dismiss];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [SVProgressHUD showErrorWithStatus:@"Не удалось загрузить список фильмов"];
        NSLog(@"%@", error);
    }];
}

+ (void)downloadDetailFilm:(NSString *)filmID block:(void(^)(NSDictionary *completion))completion {
    [SVProgressHUD show];
    
    NSString *urlFilm = [NSString stringWithFormat:@"http://api.kinopoisk.cf/getFilm?filmID=%@", filmID];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:urlFilm parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"%@", responseObject);
        @try {
           @autoreleasepool {
               NSString *strImage = [NSString stringWithFormat:@"https://st.kp.yandex.net/images/film_big/%@.jpg", responseObject[@"filmID"]];
               NSString *strUrlVideo = responseObject[@"videoURL"][@"hd"] ? responseObject[@"videoURL"][@"hd"]:responseObject[@"videoURL"][@"sd"];
               
               NSString *strRaitingMPAA = responseObject[@"ratingMPAA"] ? responseObject[@"ratingMPAA"]:@"";
               NSString *strRaitingLimits = responseObject[@"ratingAgeLimits"] ? [NSString stringWithFormat:@"%@+", responseObject[@"ratingAgeLimits"]]:@"";
               NSString *strRaiting = [NSString stringWithFormat:@"%@ %@", strRaitingMPAA, strRaitingLimits];
               
               NSString *strSlogan = [NSString stringWithFormat:@"%@", responseObject[@"slogan"] ? [NSString stringWithFormat:@"“%@”", responseObject[@"slogan"]]:@""];
               
               NSString *strGenre = responseObject[@"genre"];
               NSString *strLength = responseObject[@"filmLength"] ? responseObject[@"filmLength"]:@"--";
               NSString *strCountry = responseObject[@"country"];
               NSString *strGenreCountryDur = [NSString stringWithFormat:@"%@, %@, %@ ч.", strGenre, strCountry, strLength];
               
               NSString *strNameEn = responseObject[@"nameEN"] ? responseObject[@"nameEN"]:@"";
               NSString *strYear = responseObject[@"year"];
               NSString *strNameENYear = [NSString stringWithFormat:@"%@ (%@)", strNameEn, strYear];
               
               NSString *strDecription = responseObject[@"description"] ? responseObject[@"description"]:@"";
               
               completion(@{@"NameRU": responseObject[@"nameRU"],
                            @"NameEN": strNameENYear,
                            @"Image": strImage,
                            @"Video": strUrlVideo ? strUrlVideo:@"",
                            @"Slogan": strSlogan,
                            @"GenreCountryDur": strGenreCountryDur,
                            @"Description": strDecription,
                            @"Raiting": strRaiting,
                            @"RaitingData": responseObject[@"ratingData"],
                            @"ImdbID": responseObject[@"imdbID"] ? responseObject[@"imdbID"]:@"",
                            @"RentData": responseObject[@"rentData"],
                            @"Gallery": responseObject[@"gallery"] ? responseObject[@"gallery"]:@"",
                            @"BudgetData": responseObject[@"budgetData"] ? responseObject[@"budgetData"]:@"",
                            @"Creators": responseObject[@"creators"] ? responseObject[@"creators"]:@"",
                            @"Seance": ([responseObject[@"hasSeance"] integerValue] == 1) ? @1:@0,
                            @"WebUrl": responseObject[@"webURL"] ? responseObject[@"webURL"]:@"http://www.kinopoisk.ru"});
           }
            
            [SVProgressHUD dismiss];
        } @catch (NSException *exception) {
            [SVProgressHUD showErrorWithStatus:@"Не удалось загрузить фильм!"];
            //NSLog(@"%@", exception.description);
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        } @finally {

        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
        //NSLog(@"%@", error);
    }];
}

+ (void)downloadSeanseFilm:(NSString *)filmID filmInCity:(NSString *)cityID filmInDate:(NSString *)date block:(void(^)(NSDictionary *completion))completion {
    [SVProgressHUD show];
    
    NSString *urlSeanse = [NSString stringWithFormat:@"http://api.kinopoisk.cf/getSeance?filmID=%@&cityID=%@&date=%@", filmID, cityID, date];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:urlSeanse parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"%@", responseObject);
        @autoreleasepool {
            NSMutableArray *arrayAddress = [NSMutableArray array];
            NSMutableArray *arrayLocation = [NSMutableArray array];
            NSMutableDictionary *dictTime = [NSMutableDictionary dictionary];
            NSMutableDictionary *dictTime3D = [NSMutableDictionary dictionary];
            NSMutableDictionary *dictTime2D = [NSMutableDictionary dictionary];
            @try {
                if (responseObject[@"items"]) {
                    [responseObject[@"items"] enumerateObjectsUsingBlock:^(id  _Nonnull objItems, NSUInteger idx, BOOL * _Nonnull stop) {
                        NSMutableArray *array = [NSMutableArray array];
                        NSMutableSet *indexSet3D = [NSMutableSet set];
                        NSMutableSet *indexSet2D = [NSMutableSet set];
                        
                        [arrayAddress addObject:(objItems[@"address"])?objItems[@"address"]:@"---"];
                        
                        if (objItems[@"lat"] && objItems[@"lon"]) {
                            [arrayLocation addObject:@{@"lat":objItems[@"lat"], @"lon":objItems[@"lon"]}];
                        }
                        
                        if (objItems[@"seance"]) {
                            [objItems[@"seance"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                [array addObject:[obj isKindOfClass:[NSDictionary class]] ? obj[@"time"]:obj];
                            }];
                        }
                        if (objItems[@"seance3D"]) {
                            [objItems[@"seance3D"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                [array addObject:[obj isKindOfClass:[NSDictionary class]] ? obj[@"time"]:obj];
                            }];
                        }
                        
                        if (objItems[@"seance3D"]) {
                            [objItems[@"seance3D"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                [indexSet3D addObject:[obj isKindOfClass:[NSDictionary class]] ? obj[@"time"]:obj];
                            }];
                            [dictTime3D setValue:indexSet3D forKey:objItems[@"cinemaName"]];
                        }
                        if (objItems[@"seance"]) {
                            [objItems[@"seance"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                [indexSet2D addObject:[obj isKindOfClass:[NSDictionary class]] ? obj[@"time"]:obj];
                            }];
                            [dictTime2D setValue:indexSet2D forKey:objItems[@"cinemaName"]];
                        }
                        
                        NSArray *sortArray = [array sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                            return [obj1 compare:obj2];
                        }];
                        [dictTime setValue:sortArray forKey:objItems[@"cinemaName"]];
                    }];
                    
                    completion(@{@"Title": [NSString stringWithFormat:@"Сеансы: %@", responseObject[@"nameRU"]],
                                 @"Address": arrayAddress,
                                 @"Location": arrayLocation,
                                 @"Seanse": dictTime,
                                 @"Seanse2D": dictTime2D,
                                 @"Seanse3D": dictTime3D});
                } else {
                    [SVProgressHUD showWithStatus:@"На эту дату сеансов нет!"];
                }
            } @catch (NSException *exception) {
                //NSLog(@"%@", exception.description);
                [SVProgressHUD showWithStatus:@"Не удалось загрузить список сеансов!"];
            } @finally {
                [SVProgressHUD dismiss];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
        //NSLog(@"%@", error);
    }];
}

+ (void)downloadCityByCountry:(NSString *)country block:(void(^)(NSDictionary *completion))completion {
    [SVProgressHUD show];
    
    NSString *urlCity = [NSString stringWithFormat:@"http://api.kinopoisk.cf/getCityList?countryID=%@", country];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:urlCity parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"%@", responseObject);
        @autoreleasepool {
            NSMutableDictionary *dictName = [NSMutableDictionary dictionary];
            __block NSMutableDictionary *dictID = [NSMutableDictionary dictionary];
            @try {
                [responseObject[@"cityData"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    dictID = [KSFilter addNameCityInDict:dictID valueDict:obj[@"cityID"] keyDict:[obj[@"cityName"] substringToIndex:1]];
                    [dictName setValue:obj[@"cityName"] forKey:obj[@"cityID"]];
                }];
                
                completion(@{@"City": dictName,
                             @"ID": dictID});
                
            } @catch (NSException *exception) {
                [SVProgressHUD showErrorWithStatus:@"Не удалось загрузить список городов"];
            } @finally {
                [SVProgressHUD dismiss];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
        //NSLog(@"%@", error);
    }];
}

+ (void)downloadGallary:(NSString *)filmID block:(void(^)(NSDictionary *completion))completion {
    [SVProgressHUD show];
    
    NSString *urlCity = [NSString stringWithFormat:@"http://api.kinopoisk.cf/getGallery?filmID=%@", filmID];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:urlCity parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"%@", responseObject);
        @autoreleasepool {
            NSMutableDictionary *dictKadr = [NSMutableDictionary dictionary];
            NSMutableDictionary *dictKadrSP = [NSMutableDictionary dictionary];
            NSMutableDictionary *dictPoster = [NSMutableDictionary dictionary];
            NSMutableArray *arrayImage = [NSMutableArray array];
            NSMutableArray *arrayPreview = [NSMutableArray array];
            @try {
                [responseObject[@"gallery"][@"kadr"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [arrayImage addObject:obj[@"image"]];
                    [arrayPreview addObject:obj[@"preview"]];
                }];
                [dictKadr setValue:arrayPreview forKey:@"Preview"];
                [dictKadr setValue:arrayImage forKey:@"Image"];
                
                [responseObject[@"gallery"][@"kadr_sp"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [arrayImage addObject:obj[@"image"]];
                    [arrayPreview addObject:obj[@"preview"]];
                }];
                [dictKadrSP setValue:arrayPreview forKey:@"Preview"];
                [dictKadrSP setValue:arrayImage forKey:@"Image"];
                
                [responseObject[@"gallery"][@"poster"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [arrayImage addObject:obj[@"image"]];
                    [arrayPreview addObject:obj[@"preview"]];
                }];
                [dictPoster setValue:arrayPreview forKey:@"Preview"];
                [dictPoster setValue:arrayImage forKey:@"Image"];
                
                completion(@{@"Kadr": dictKadr,
                             @"KadrSP": dictKadrSP,
                             @"Poster": dictPoster});
                
            } @catch (NSException *exception) {
                //NSLog(@"%@", exception.description);
                [SVProgressHUD showErrorWithStatus:@"Не удалось загрузить список городов"];
            } @finally {
                [SVProgressHUD dismiss];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
        //NSLog(@"%@", error);
    }];
}

+ (void)downloadFilter:(void(^)(NSDictionary *completion))completion {
    [SVProgressHUD show];
    
    NSString *urlGenre = @"http://api.kinopoisk.cf/getGenres";
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:urlGenre parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"%@", responseObject);
        @autoreleasepool {
            NSMutableArray *arrayName = [NSMutableArray array];
            NSMutableArray *arrayID = [NSMutableArray array];
            @try {
                [responseObject[@"genreData"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                    [arrayName addObject:obj[@"genreName"]];
                    [arrayID addObject:obj[@"genreID"]];
                }];
                
                completion(@{@"Name": arrayName,
                             @"ID": arrayID});
                
                [SVProgressHUD dismiss];
            } @catch (NSException *exception) {
                [SVProgressHUD showErrorWithStatus:@"Не удалось загрузить список жанров!"];
                //NSLog(@"%@", exception.description);
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                });
            } @finally {}
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
    }];
}

@end
