//
//  KSCalenderViewController.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 09.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSCalenderViewController.h"

@interface KSCalenderViewController ()

@end

@implementation KSCalenderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - FSCalendar Delegate

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CalendarDate" object:@{@"date":date}];
}

- (void)calendar:(FSCalendar *)calendar didDeselectDate:(NSDate *)date {
    [calendar deselectDate:date];
}

@end
