//
//  KSCustomView.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 20.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSCustomView.h"

@implementation KSCustomView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    //3a7bd5
    gradient.colors = @[(id)[UIColor colorWithHexString:@"4b79a1"].CGColor, (id)[UIColor colorWithHexString:@"283e51"].CGColor];
    //gradient.locations = @[@0.0, @0.3, @1.0];
    
    [self.layer insertSublayer:gradient atIndex:0];
}

@end
