//
//  KSCustomButton.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 20.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSCustomButton.h"

@implementation KSCustomButton

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    self.layer.borderWidth = 2.f;
    self.layer.borderColor = [UIColor blackColor].CGColor;
    self.layer.cornerRadius = self.frame.size.width/2;
    self.layer.masksToBounds = TRUE;
}

@end
