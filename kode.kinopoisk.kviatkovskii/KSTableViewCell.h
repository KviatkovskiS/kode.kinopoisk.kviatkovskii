//
//  KSTableViewCell.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 19.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageMainCell;
@property (weak, nonatomic) IBOutlet UILabel *titleMainCell;
@property (weak, nonatomic) IBOutlet UILabel *raitingMainCell;
@property (weak, nonatomic) IBOutlet UILabel *countryMainCell;
@property (weak, nonatomic) IBOutlet UIView *backgroundMainCell;
@property (weak, nonatomic) IBOutlet UILabel *genreMainCell;

@property (weak, nonatomic) IBOutlet UILabel *titleSortCell;
@property (weak, nonatomic) IBOutlet UIImageView *imageSortCell;
@end
