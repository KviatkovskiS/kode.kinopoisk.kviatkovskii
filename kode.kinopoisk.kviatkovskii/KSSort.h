//
//  KSSort.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 26.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KSSort : NSObject

+ (NSArray *)sortDictionary:(NSMutableDictionary *)dict sortParameter:(NSInteger)parameter;

@end
