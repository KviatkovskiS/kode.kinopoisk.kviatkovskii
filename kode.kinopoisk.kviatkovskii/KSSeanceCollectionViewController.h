//
//  KSSeanceCollectionViewController.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 28.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSCollectionViewCell.h"
#import "KSDate.h"
#import "KSLoader.h"
#import "KSCollectionReusableView.h"
#import "KSMapViewController.h"

@interface KSSeanceCollectionViewController : UICollectionViewController <UICollectionViewDelegateFlowLayout>

@end
