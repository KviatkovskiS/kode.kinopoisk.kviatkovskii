//
//  KSDetailFilmViewController.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 28.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSDetailFilmViewController.h"

@interface KSDetailFilmViewController () {
    NSString *strUrlVideo;
    NSMutableDictionary *dictDetail;
    NSMutableSet *bigCells;
    NSArray *arraySection;
    NSArray *arrayDistributors;
    NSNumber *hasSeance;
    NSString *webUrl;
}

@end

@implementation KSDetailFilmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dictDetail = [NSMutableDictionary dictionary];
    bigCells = [NSMutableSet set];
    
    self.navigationItem.title = _titleNavBar;
    
    [self downloadDetailFilm:_IDFilm];
    
    arraySection = @[@"Расписание", @"В прокате", @"Описание", @"Ссылки"];
    [dictDetail setValue:@[@"Открыть на КиноПоиске", @"Поделиться ссылкой"] forKey:@"Ссылки"];

    self.tableView.tableHeaderView = self.headerView;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        self.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    AppDelegate *shared = (AppDelegate*)[UIApplication sharedApplication].delegate;
    shared.blockRotation = FALSE;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    AppDelegate *shared = (AppDelegate*)[UIApplication sharedApplication].delegate;
    shared.blockRotation = TRUE;
}

- (void)viewWillLayoutSubviews {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.headerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 600);
    }
    else {
        self.headerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 670);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)downloadDetailFilm:(NSString *)filmID {    
    __weak typeof(self) weakSelf = self;
    
    [KSLoader downloadDetailFilm:filmID block:^(NSDictionary *completion) {
        //NSLog(@"%@", completion);
        weakSelf.raitingMPAALbl.text = completion[@"Raiting"];
        weakSelf.titleFilmLbl.text = completion[@"NameRU"];
        weakSelf.enTitleFilmLbl.text = completion[@"NameEN"];
        
        if (![completion[@"Slogan"] isEqualToString:@""]) {
            weakSelf.sloganLbl.text = completion[@"Slogan"];
        } else {
            weakSelf.sloganLbl.hidden = TRUE;
        }
        
        weakSelf.genreCountryDurationLbl.text = completion[@"GenreCountryDur"];
        [weakSelf.blurImage sd_setImageWithURL:[NSURL URLWithString:completion[@"Image"]] placeholderImage:nil options:SDWebImageHighPriority];
        [weakSelf.imageHeader sd_setImageWithURL:[NSURL URLWithString:completion[@"Image"]] placeholderImage:nil options:SDWebImageHighPriority];
        
        if (![completion[@"Video"] isEqualToString:@""]) {
            strUrlVideo = completion[@"Video"];
        } else {
            weakSelf.playTrailerBtn.hidden = TRUE;
        }
        
        [dictDetail setValue:@[@"Сеансы"] forKey:@"Расписание"];
        [dictDetail setValue:@[completion[@"Description"]] forKey:@"Описание"];

        @autoreleasepool {
            NSMutableArray *arrayRent = [NSMutableArray array];
            
            NSString *rentStringRU = [NSString stringWithFormat:@"Премьера (Россия) - "];
            NSMutableAttributedString *mutStringRU = [[NSMutableAttributedString alloc] initWithString:rentStringRU
                                                                                            attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
            NSString *rentStringEN = [NSString stringWithFormat:@"Премьера (мир) - "];
            NSMutableAttributedString *mutStringEN = [[NSMutableAttributedString alloc] initWithString:rentStringEN
                                                                                            attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
            
            NSAttributedString *attStringRUDate = [[NSAttributedString alloc] initWithString:completion[@"RentData"][@"premiereRU"] ? completion[@"RentData"][@"premiereRU"]:@""
                                                                                  attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16],
                                                                                               NSForegroundColorAttributeName:[UIColor blackColor]}];
            NSAttributedString *attStringENDate = [[NSAttributedString alloc] initWithString:completion[@"RentData"][@"premiereWorld"] ? completion[@"RentData"][@"premiereWorld"]:@""
                                                                                  attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16],
                                                                                               NSForegroundColorAttributeName:[UIColor blackColor]}];
            
            [mutStringRU appendAttributedString:attStringRUDate];
            [mutStringEN appendAttributedString:attStringENDate];
            
            [arrayRent insertObject:mutStringRU atIndex:0];
            [arrayRent insertObject:mutStringEN atIndex:1];
            [dictDetail setValue:arrayRent forKey:@"В прокате"];
            NSAttributedString *attDistributorsRU = [[NSAttributedString alloc] initWithString:completion[@"RentData"][@"Distributors"] ?completion[@"RentData"][@"Distributors"]:@""
                                                                                    attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
            NSAttributedString *attDistributorsWorld = [[NSAttributedString alloc] initWithString:completion[@"RentData"][@"premiereWorldCountry"] ?completion[@"RentData"][@"premiereWorldCountry"]:@""
                                                                                       attributes:@{NSForegroundColorAttributeName:[UIColor darkGrayColor]}];
            
            arrayDistributors = @[attDistributorsRU, attDistributorsWorld];
        }
    
        hasSeance = completion[@"Seance"];
        webUrl = completion[@"WebUrl"];
        
        [weakSelf.tableView reloadData];
    }];
}

#pragma mark - Button Action

- (IBAction)pressPlayBtn:(id)sender {
    [SVProgressHUD show];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrlVideo]];
    [self.webView loadRequest:request];
}

#pragma mark - WebView Delegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [SVProgressHUD dismiss];
    self.webView.hidden = FALSE;
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arraySection.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return  arraySection[section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *sectionTitle = arraySection[section];
    NSArray *array = dictDetail[sectionTitle];
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    static NSString *identifierRent = @"CellRent";
    
    UITableViewCell *cell;
    
    NSString *sectionTitle = arraySection[indexPath.section];
    NSArray *array = dictDetail[sectionTitle];
    
    if (indexPath.section == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:identifierRent forIndexPath:indexPath];
        
        cell.textLabel.attributedText = array[indexPath.row];
        cell.detailTextLabel.attributedText = arrayDistributors[indexPath.row];
    }
    else {
        cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        cell.textLabel.text = array[indexPath.row];
    }
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        if ([hasSeance isEqual:@0]) {
            cell.userInteractionEnabled = FALSE;
            cell.textLabel.alpha = .8f;
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (indexPath.section == 2 && indexPath.row == 0) {
        cell.textLabel.numberOfLines = 0;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == 2 && indexPath.row == 0) {
        if ([bigCells containsObject:indexPath]) {
            [bigCells removeObject:indexPath];
        }
        else {
            [bigCells addObject:indexPath];
        }
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    else if (indexPath.section == 0 && indexPath.row == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:@{@"filmID":_IDFilm,
                                                           @"offsetDay":_offsetDay,
                                                           @"Date":[KSDate convertStringDateToYandexDate:_titleNavBar]} forKey:@"SeanceFilm"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        KSDetailFilmViewController *seanceView = [self.storyboard instantiateViewControllerWithIdentifier:@"seanceView"];
        [self.navigationController pushViewController:seanceView animated:TRUE];
    }
    else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            NSURL *url = [NSURL URLWithString:webUrl];
            [[UIApplication sharedApplication] openURL:url];
        } else if (indexPath.row == 1) {
            NSString *text = [NSString stringWithFormat:@"%@ (%@)", self.titleFilmLbl.text, self.enTitleFilmLbl.text];
            
            NSURL  *url = [NSURL URLWithString:webUrl];
            NSArray *items = @[self.imageHeader.image, text, url];
            
            UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:nil];
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                UIPopoverPresentationController *popoverController = activityController.popoverPresentationController;
                if ([activityController respondsToSelector:@selector(popoverPresentationController)]) {
                    popoverController.sourceView = tableView;
                    popoverController.sourceRect = cell.frame;
                    popoverController.permittedArrowDirections = UIPopoverArrowDirectionAny;
                }
                [self presentViewController:activityController animated:YES completion:nil];
            } else {
                [self presentViewController:activityController animated:YES completion:nil];
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2 && indexPath.row == 0) {
        if ([bigCells containsObject:indexPath]) {
            NSString *sectionTitle = arraySection[indexPath.section];
            NSArray *array = dictDetail[sectionTitle];
            
            CGFloat height = [array[indexPath.row] boundingRectWithSize:CGSizeMake(tableView.frame.size.width, 1000)
                                                                      options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                                                   attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:19]}
                                                                      context:nil].size.height;
            
            return height + 15;
        }
        else {
            return 100.f;
        }
    }
    return 44.f;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"galleryView"]) {
        KSGalleryFilmViewController *destination = segue.destinationViewController;
        destination.filmID = _IDFilm;
    }
    if ([segue.identifier isEqualToString:@"ratingView"]) {
        KSRatingFilmViewController *destination = segue.destinationViewController;
        destination.rateKinopoisk = _ratingKinopoisk;
        destination.filmID = _IDFilm;
    }
}

@end
