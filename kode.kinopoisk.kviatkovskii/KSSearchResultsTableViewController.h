//
//  KSSearchResultsTableViewController.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 04.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchCityDelegate <NSObject>

- (void)searchCityID:(NSString *)cityID cityName:(NSString *)cityName;

@end

@interface KSSearchResultsTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *searchResults;
@property (nonatomic, strong) NSArray *searchResultsID;
@property (nonatomic, weak) id <SearchCityDelegate> delegate;

@end
