//
//  KSDetailFilmViewController.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 28.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "KSLoader.h"
#import "KSLoaderIMDB.h"
#import "KSDate.h"
#import "KSGalleryFilmViewController.h"
#import "KSCustomButton.h"
#import "KSRatingFilmViewController.h"
#import "AppDelegate.h"

@interface KSDetailFilmViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIWebViewDelegate, UIApplicationDelegate>

@property (strong, nonatomic) NSString *IDFilm;
@property (strong, nonatomic) NSString *offsetDay;
@property (strong, nonatomic) NSString *titleNavBar;
@property (strong, nonatomic) NSString *ratingKinopoisk;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *imageHeader;
@property (weak, nonatomic) IBOutlet UIImageView *blurImage;
@property (weak, nonatomic) IBOutlet UILabel *raitingMPAALbl;
@property (weak, nonatomic) IBOutlet UILabel *titleFilmLbl;
@property (weak, nonatomic) IBOutlet UILabel *enTitleFilmLbl;
@property (weak, nonatomic) IBOutlet UILabel *genreCountryDurationLbl;
@property (weak, nonatomic) IBOutlet UILabel *sloganLbl;
//@property (weak, nonatomic) IBOutlet UIView *galleryView;
@property (weak, nonatomic) IBOutlet KSCustomButton *playTrailerBtn;

@end
