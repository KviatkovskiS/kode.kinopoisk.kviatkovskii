//
//  KSBlurImage.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 05.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSBlurImage.h"

@implementation KSBlurImage

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UIBlurEffect *blurDark = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *darkEffectView = [[UIVisualEffectView alloc] initWithEffect:blurDark];
    [darkEffectView setFrame:self.frame];
    darkEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self addSubview:darkEffectView];
}

@end
