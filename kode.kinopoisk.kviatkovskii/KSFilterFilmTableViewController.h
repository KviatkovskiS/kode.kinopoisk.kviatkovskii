//
//  KSFilterFilmTableViewController.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 19.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSLoader.h"

@interface KSFilterFilmTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSCoding>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
