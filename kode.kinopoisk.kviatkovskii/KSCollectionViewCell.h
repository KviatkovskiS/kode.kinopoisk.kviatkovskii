//
//  KSCollectionViewCell.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 19.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KSCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleHeaderCell;
@property (weak, nonatomic) IBOutlet UIImageView *imageHeaderCell;

@property (weak, nonatomic) IBOutlet UIView *backgroundSeanceCell;
@property (weak, nonatomic) IBOutlet UILabel *titleSeanceCell;
@property (weak, nonatomic) IBOutlet UILabel *lbl3dSeanceCell;
@property (weak, nonatomic) IBOutlet UILabel *lbl2dSeanceCell;

@property (weak, nonatomic) IBOutlet UIImageView *galleryImageCell;

@property (weak, nonatomic) IBOutlet UILabel *titleRatingCell;
@property (weak, nonatomic) IBOutlet UILabel *detailRatingCell;
@end
