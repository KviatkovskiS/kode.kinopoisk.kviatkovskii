//
//  ViewController.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 19.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSMenuCityViewController.h"
#import "NSObject+KSColor_hex.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "KSDate.h"
#import "KSTableViewCell.h"
#import "KSSort.h"
#import "KSFilter.h"
#import "KSHeaderMainViewCollectionViewController.h"
#import "KSDetailFilmViewController.h"
#import "KSLoader.h"
#import "AppDelegate.h"

@interface ViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CityDelegate, FilterDelegate, UIApplicationDelegate, UIPopoverPresentationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *cityBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorActivity;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *loadMoreBtn;
@property (weak, nonatomic) IBOutlet UIButton *calendarBtn;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barRightBtn;

@end

