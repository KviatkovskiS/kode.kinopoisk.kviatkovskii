//
//  KSCollectionViewCell.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 19.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSCollectionViewCell.h"

@implementation KSCollectionViewCell

- (void)drawRect:(CGRect)rect {
    [self cornerRadiusView:self.backgroundSeanceCell cornerWidth:10.f cornerHeight:10.f];
}

- (void)cornerRadiusView:(UIView *)view cornerWidth:(float)width cornerHeight:(float)height {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                   byRoundingCorners:UIRectCornerAllCorners
                                                         cornerRadii:CGSizeMake(width, height)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
    view.layer.masksToBounds = TRUE;
}

@end
