//
//  KSFilter.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 27.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KSFilter : NSObject

+ (BOOL)containsArray:(NSArray *)array genreString:(NSString *)genre;
+ (id)addNameCityInDict:(id)dict valueDict:(id)value keyDict:(id)key;

@end
