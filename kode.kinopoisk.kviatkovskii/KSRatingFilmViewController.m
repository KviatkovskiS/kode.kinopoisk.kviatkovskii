//
//  KSRatingFilmViewController.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 06.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSRatingFilmViewController.h"

@interface KSRatingFilmViewController () {
    NSArray *arrayTitle;
    NSMutableArray *arrayRating;
}

@end

@implementation KSRatingFilmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    __weak typeof(self) weakSelf = self;
    
    arrayTitle = @[@"Кинопоиск", @"IMDB"];
    arrayRating = [NSMutableArray array];

    [KSLoader downloadDetailFilm:_filmID block:^(NSDictionary *completion) {
        [KSLoaderIMDB downloadIMDBShortData:completion[@"ImdbID"] block:^(NSDictionary *completion) {
            [arrayRating insertObject:_rateKinopoisk atIndex:0];
            [arrayRating insertObject:completion[@"RatingIMDB"] atIndex:1];
            [weakSelf.collectionView reloadData];
        }];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collection View

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrayRating.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    
    KSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.titleRatingCell.text = arrayTitle[indexPath.row];
    cell.detailRatingCell.text = arrayRating[indexPath.row];
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if (result.height <= 568) {
            return CGSizeMake(140, 60);
        }
        if (result.height == 667) {
            return CGSizeMake(150, 70);
        }
        if (result.height == 736) {
            return CGSizeMake(150, 70);
        }
    }
    else {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if (result.height == 1366 || result.width == 1366) {
            return CGSizeMake(400, 70);
        } else {
            return CGSizeMake(300, 70);
        }
    }
    return CGSizeMake(0, 0);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if (result.height <= 568) {
            return UIEdgeInsetsMake(0, 5, 0, 5);
        }
        if (result.height == 667) {
            return UIEdgeInsetsMake(0, 20, 0, 20);
        }
        if (result.height == 736) {
            return UIEdgeInsetsMake(0, 40, 0, 40);
        }
    }
    else {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if (result.height == 1366 || result.width == 1366) {
            return UIEdgeInsetsMake(0, 80, 0, 80);
        } else {
            return UIEdgeInsetsMake(0, 50, 0, 50);
        }
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

@end
