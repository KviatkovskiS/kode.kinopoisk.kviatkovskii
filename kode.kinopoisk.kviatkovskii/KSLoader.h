//
//  KSLoader.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 02.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "KSDate.h"
#import "KSFilter.h"
#import "KSSort.h"

@interface KSLoader : NSObject

+ (void)downloadFilmByCity:(NSString *)cityID filmDate:(NSString *)date filmSort:(NSInteger)parameter genreFilter:(NSString *)genreName block:(void(^)(NSDictionary *completion))completion;
+ (void)downloadDetailFilm:(NSString *)filmID block:(void(^)(NSDictionary *completion))completion;
+ (void)downloadSeanseFilm:(NSString *)filmID filmInCity:(NSString *)cityID filmInDate:(NSString *)date block:(void(^)(NSDictionary *completion))completion;
+ (void)downloadCityByCountry:(NSString *)country block:(void(^)(NSDictionary *completion))completion;
+ (void)downloadGallary:(NSString *)filmID block:(void(^)(NSDictionary *completion))completion;
+ (void)downloadFilter:(void(^)(NSDictionary *completion))completion;

@end
