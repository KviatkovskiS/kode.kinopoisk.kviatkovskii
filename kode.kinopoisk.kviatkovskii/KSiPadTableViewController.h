//
//  KSiPadTableViewController.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 08.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSMenuCityViewController.h"
#import "NSObject+KSColor_hex.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "KSDate.h"
#import "KSTableViewCell.h"
#import "KSSort.h"
#import "KSFilter.h"
#import "KSHeaderMainViewCollectionViewController.h"
#import "KSDetailFilmViewController.h"
#import "KSLoader.h"
#import "AppDelegate.h"

@interface KSiPadTableViewController : UITableViewController <CityDelegate, FilterDelegate>

@property (weak, nonatomic) IBOutlet UIButton *cityBtn;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorActivity;
@property (strong, nonatomic) IBOutlet UIView *footerView;
@property (weak, nonatomic) IBOutlet UIButton *loadMoreBtn;
@property (strong, nonatomic) IBOutlet UIView *headerView;

@end
