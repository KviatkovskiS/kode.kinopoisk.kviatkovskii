//
//  KSDate.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 20.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KSDate : NSObject

+ (NSString *)getNextDay:(NSInteger)count curDate:(NSDate *)date;
+ (NSString *)convertYandexDateToStringDate:(NSString *)date;
+ (NSString *)convertStringDateToYandexDate:(NSString *)date;
+ (BOOL)nowTimeCompareSeanceTime:(NSString *)time;

@end
