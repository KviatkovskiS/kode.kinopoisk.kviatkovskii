//
//  KSGalleryFilmViewController.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 05.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSCollectionViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "KSLoader.h"
#import "IDMPhoto.h"
#import "IDMPhotoBrowser.h"

@interface KSGalleryFilmViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (strong, nonatomic) NSString *filmID;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionVew;

@end
