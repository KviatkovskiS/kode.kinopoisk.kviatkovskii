//
//  KSMapViewController.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 29.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSMapViewController.h"

@interface KSMapViewController ()

@end

@implementation KSMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = _titleCinema;
    
    float spanX = 0.05;
    float spanY = 0.05;

    MKCoordinateRegion region;
    region.center.latitude = [_dictCoordinates[@"lat"] floatValue];
    region.center.longitude = [_dictCoordinates[@"lon"] floatValue];
    region.span.latitudeDelta = spanX;
    region.span.longitudeDelta = spanY;
    [self.mapView setRegion:region animated:YES];
    
    MKPointAnnotation *myAnnotation = [[MKPointAnnotation alloc] init];
    myAnnotation.coordinate = CLLocationCoordinate2DMake([_dictCoordinates[@"lat"] floatValue], [_dictCoordinates[@"lon"] floatValue]);
    myAnnotation.title = _titleCinema;
    myAnnotation.subtitle = _addressCinema;
    
    [self.mapView addAnnotation:myAnnotation];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    MKPinAnnotationView *view = nil;
    static NSString *reuseIdentifier = @"MapAnnotation";
    
    // Return a MKPinAnnotationView with a simple accessory button
    view = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseIdentifier];
    if(!view) {
        view = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
        view.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];;
        view.canShowCallout = YES;
        view.animatesDrop = YES;
    }
    
    return view;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:CLLocationCoordinate2DMake([_dictCoordinates[@"lat"] floatValue], [_dictCoordinates[@"lon"] floatValue])
                                                   addressDictionary:nil];
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    [mapItem setName:_titleCinema];
    NSDictionary *options = @{MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeDriving};
    [mapItem openInMapsWithLaunchOptions:options];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressCloseBtn:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

@end
