//
//  NSObject+KSColor_hex.h
//  SerVik
//
//  Created by Kviatkovskii on 29.09.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIColor (KSColor_hex)

+ (UIColor *)colorWithHexString:(NSString *)hexString;

@end
