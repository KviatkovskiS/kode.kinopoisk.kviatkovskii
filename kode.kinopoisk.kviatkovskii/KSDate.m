//
//  KSDate.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 20.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSDate.h"

@implementation KSDate

+ (NSString *)getNextDay:(NSInteger)count curDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *tomorrow = [calendar dateByAddingUnit:NSCalendarUnitDay value:count toDate:date options:0];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:tomorrow];
    
    return strDate;
}

+ (NSString *)convertYandexDateToStringDate:(NSString *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    
    NSDate *dateFrom = [dateFormatter dateFromString:date];
    
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    NSString *strDate = [dateFormatter stringFromDate:dateFrom];
    return strDate;
}

+ (NSString *)convertStringDateToYandexDate:(NSString *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    
    NSDate *dateFrom = [dateFormatter dateFromString:date];
    
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    NSString *strDate = [dateFormatter stringFromDate:dateFrom];
    return strDate;
}

+ (BOOL)nowTimeCompareSeanceTime:(NSString *)time {
    NSDate *today = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];

    NSDate *dateFrom = [dateFormatter dateFromString:time];
    
    NSComparisonResult result = [today compare:dateFrom];
    
    if(result == NSOrderedAscending)
        return TRUE;
    else if(result == NSOrderedDescending)
        return FALSE;
    else if(result == NSOrderedSame)
        return FALSE;
    else
        return 0;
}

@end
