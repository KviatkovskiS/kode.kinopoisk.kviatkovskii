//
//  KSLoaderIMDB.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 05.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSLoaderIMDB.h"

@implementation KSLoaderIMDB

//http://www.omdbapi.com/?i=ImbdID&plot=short&r=json

+ (void)downloadIMDBShortData:(NSString *)ttIMDB block:(void(^)(NSDictionary *completion))completion {
    NSString *url = [NSString stringWithFormat:@"http://www.omdbapi.com/?i=%@&plot=short&r=json", ttIMDB];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"%@", responseObject);
        @autoreleasepool {
            NSString *strRating = responseObject[@"imdbRating"] ? responseObject[@"imdbRating"]:@"--";
            NSString *strVotes = responseObject[@"imdbVotes"] ? responseObject[@"imdbVotes"]:@"--";
            NSString *strIMDB = [NSString stringWithFormat:@"%@ (%@)", strRating, strVotes];
            completion(@{@"RatingIMDB": strIMDB});
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        //NSLog(@"%@", error);
    }];
}

@end
