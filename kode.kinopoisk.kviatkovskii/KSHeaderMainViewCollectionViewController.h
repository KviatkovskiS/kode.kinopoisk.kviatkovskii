//
//  KSHeaderMainViewCollectionViewController.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 19.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSCollectionViewCell.h"

@protocol FilterDelegate <NSObject>

- (void)listGenreName:(NSMutableArray *)arrayName;

@end

@interface KSHeaderMainViewCollectionViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UIPopoverPresentationControllerDelegate>

@property (weak, nonatomic) id <FilterDelegate> delegate;

@end
