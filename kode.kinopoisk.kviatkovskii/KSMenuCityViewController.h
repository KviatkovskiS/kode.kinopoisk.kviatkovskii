//
//  KSMenuCityViewController.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 19.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "KSLoader.h"
#import "KSSearchResultsTableViewController.h"

@protocol CityDelegate <NSObject>

- (void)getCity:(NSString *)cityID nameCity:(NSString *)name;
- (void)saveCity;

@end

@interface KSMenuCityViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, UISearchResultsUpdating, UISearchControllerDelegate, UISearchBarDelegate, SearchCityDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) id <CityDelegate> delegate;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) NSMutableArray *searchResults;

@end
