//
//  KSGalleryFilmViewController.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 05.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSGalleryFilmViewController.h"

@interface KSGalleryFilmViewController () {
    NSMutableArray *arrayImage;
    NSMutableArray *arrayIDMPhoto;
}

@end

@implementation KSGalleryFilmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayImage = [NSMutableArray array];
    arrayIDMPhoto = [NSMutableArray array];
    
    __weak typeof(self) weakSelf = self;
    
    [KSLoader downloadGallary:_filmID block:^(NSDictionary *completion) {
     //NSLog(@"%@", completion[@"Kadr"]);
        [completion[@"Kadr"][@"Preview"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [arrayImage addObject:[NSURL URLWithString:[NSString stringWithFormat:@"https://st.kp.yandex.net/images/%@", obj]]];
            [weakSelf.collectionVew reloadData];
        }];
        [completion[@"Kadr"][@"Image"] enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [arrayIDMPhoto addObject:[IDMPhoto photoWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://st.kp.yandex.net/images/%@", obj]]] ];
        }];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collection View

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrayImage.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    
    KSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    [cell.galleryImageCell sd_setImageWithURL:arrayImage[indexPath.row] placeholderImage:nil options:SDWebImageHighPriority];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    KSCollectionViewCell *cell = (KSCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:arrayIDMPhoto animatedFromView:cell.galleryImageCell];
    browser.displayActionButton = TRUE;
    browser.displayArrowButton = FALSE;
    browser.usePopAnimation = TRUE;
    browser.scaleImage = cell.galleryImageCell.image;
    [browser setInitialPageIndex:indexPath.row];
    
    [self presentViewController:browser animated:YES completion:nil];
}

@end
