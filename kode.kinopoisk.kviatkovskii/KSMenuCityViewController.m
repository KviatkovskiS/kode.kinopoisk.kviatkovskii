//
//  KSMenuCityViewController.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 19.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSMenuCityViewController.h"

@interface KSMenuCityViewController () {
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
    NSString *cityLocation;
    
    NSMutableDictionary *dictCityID;
    NSMutableDictionary *dictCityName;
    NSString *city;
    NSArray *arraySection;
    NSIndexPath *indexSelect;
    NSMutableArray *filteredItems;
    NSMutableArray *filteredItemsID;
    NSArray *displayedItems;
    NSArray *displayedItemsID;
}

@end

@implementation KSMenuCityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dictCityID = [NSMutableDictionary dictionary];
    dictCityName = [NSMutableDictionary dictionary];
    filteredItems = [[NSMutableArray alloc] init];
    filteredItemsID = [[NSMutableArray alloc] init];
    
    [self downloadCityByCountry:@"2"];
    
    //работает только если язык системы русский, отступление
    //self.detectCityBtn.hidden = TRUE;
    
    UINavigationController *searchResultsController = [[self storyboard] instantiateViewControllerWithIdentifier:@"TableSearchResultsNavigationController"];
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:searchResultsController];    
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.delegate = self;
    self.searchController.delegate = self;
    //self.searchController.dimsBackgroundDuringPresentation = FALSE;
    //self .definesPresentationContext = TRUE;
    [self.searchController.searchBar sizeToFit];
    self.tableView.tableHeaderView = self.searchController.searchBar;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Download City

- (void)downloadCityByCountry:(NSString *)country {
    __weak typeof(self) weakSelf = self;
    
    [KSLoader downloadCityByCountry:country block:^(NSDictionary *completion) {
        dictCityID = completion[@"ID"];
        dictCityName = completion[@"City"];
        
        arraySection = [[dictCityID allKeys] sortedArrayUsingSelector:@selector(compare:)];
        
        [weakSelf.tableView reloadData];
    }];
}

#pragma mark Search Controller

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    NSString *searchString = searchController.searchBar.text;
    if (![searchString isEqualToString:@""]) {
        [filteredItems removeAllObjects];
        [filteredItemsID removeAllObjects];
        
        [dictCityName enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            if ([searchString isEqualToString:@""] || [obj localizedCaseInsensitiveContainsString:searchString] == YES) {
                [filteredItems addObject:obj];
                [filteredItemsID addObject:key];
            }
        }];
        displayedItems = filteredItems;
        displayedItemsID = filteredItemsID;
    }
    else {
        displayedItems = [dictCityName allValues];
        displayedItemsID = [dictCityName allKeys];
    }
    
    if (self.searchController.searchResultsController) {
        UINavigationController *navController = (UINavigationController *) self.searchController.searchResultsController;
        KSSearchResultsTableViewController *vc = (KSSearchResultsTableViewController *) navController.topViewController;
        vc.delegate = self;
        vc.searchResults = displayedItems;
        vc.searchResultsID = displayedItemsID;
        [vc.tableView reloadData];
    }
}

#pragma mark - Search City

- (void)searchCityID:(NSString *)cityID cityName:(NSString *)cityName {
    city = cityName;
    [self.delegate getCity:cityID nameCity:cityName];
    [self pressChooseBtn:self];
    self.searchController.active = FALSE;
}

/*
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError: %@", error);
    [SVProgressHUD showErrorWithStatus:@"Не удалось определить город"];
    //[SVProgressHUD dismissWithDelay:3];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    CLLocation *currentLocation = newLocation;
    __weak typeof(self) weakSelf = self;
    
    if (currentLocation != nil) {
        //NSLog(@"%@", [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]);
        //NSLog(@"%@", [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]);
    }
    
    [locationManager stopUpdatingLocation];
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            cityLocation = placemark.locality;
            [SVProgressHUD dismiss];
            
            [arrayCityName enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                if ([cityLocation isEqualToString:obj]) {
                    [weakSelf.pickerView selectRow:idx inComponent:0 animated:TRUE];
                    [weakSelf.delegate getCity:arrayCityID[idx] nameCity:arrayCityName[idx]];
                }
                else {
                    //[SVProgressHUD showErrorWithStatus:@"Не удалось определить город"];
                }
            }];
        } else {
            //NSLog(@"%@", error.debugDescription);
        }
    }];
}
*/
- (IBAction)pressCloseBtn:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)pressChooseBtn:(id)sender {
    if (city.length != 0) {
        [self.delegate saveCity];
        [self dismissViewControllerAnimated:TRUE completion:nil];
    }
    else {
        [SVProgressHUD showInfoWithStatus:@"Выберете город!"];
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arraySection.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return  arraySection[section];
}

- (nullable NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return arraySection;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *sectionTitle = arraySection[section];
    NSArray *array = dictCityID[sectionTitle];
    
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    NSString *sectionTitle = arraySection[indexPath.section];
    NSArray *array = dictCityID[sectionTitle];
    
    cell.textLabel.text = dictCityName[array[indexPath.row]];
    
    if ((indexSelect.section == indexPath.section) && (indexSelect.row == indexPath.row) && indexSelect != NULL) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    
    NSString *sectionTitle = arraySection[indexPath.section];
    NSArray *array = dictCityID[sectionTitle];
    
    city = dictCityName[array[indexPath.row]];
    [self.delegate getCity:array[indexPath.row] nameCity:dictCityName[array[indexPath.row]]];
    
    [[tableView visibleCells] makeObjectsPerformSelector:@selector(setAccessoryType:) withObject:UITableViewCellAccessoryNone];
    
    if ([tableView cellForRowAtIndexPath:indexPath].accessoryType == UITableViewCellAccessoryNone) {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        indexSelect = indexPath;
    } else {
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        indexSelect = nil;
    }
}

/*
#pragma mark - Automatic Location

- (IBAction)pressDetectLocation:(id)sender {
    [SVProgressHUD show];
    locationManager = [[CLLocationManager alloc] init];
    geocoder = [[CLGeocoder alloc] init];
    
    locationManager.delegate = self;
    if([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]){
        NSUInteger code = [CLLocationManager authorizationStatus];
        if (code == kCLAuthorizationStatusNotDetermined && ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)] || [locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])) {
            // choose one request according to your business.
            if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationAlwaysUsageDescription"]){
                [locationManager requestAlwaysAuthorization];
            } else if([[NSBundle mainBundle] objectForInfoDictionaryKey:@"NSLocationWhenInUseUsageDescription"]) {
                [locationManager  requestWhenInUseAuthorization];
            } else {
                //NSLog(@"Info.plist does not contain NSLocationAlwaysUsageDescription or NSLocationWhenInUseUsageDescription");
            }
        }
    }
    [locationManager startUpdatingLocation];
}
*/

@end
