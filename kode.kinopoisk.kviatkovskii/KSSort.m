//
//  KSSort.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 26.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSSort.h"

@implementation KSSort

+ (NSArray *)sortDictionary:(NSMutableDictionary *)dict sortParameter:(NSInteger)parameter {
    NSArray *sortArray;
    
    if (parameter == 0) {
        sortArray = [dict keysSortedByValueUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            if ([obj1 floatValue] < [obj2 floatValue]) {
                return (NSComparisonResult)NSOrderedDescending;
            }
            return (NSComparisonResult)NSOrderedSame;
        }];
    }
    else if (parameter == 1) {
        sortArray = [dict keysSortedByValueUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            if ([obj1 floatValue] > [obj2 floatValue]) {
                return (NSComparisonResult)NSOrderedDescending;
            }
            return (NSComparisonResult)NSOrderedSame;
        }];
    }
    else if (parameter == 2) {
        sortArray = [dict allKeys];
    }

    return sortArray;
}

@end
