//
//  NSObject+KSColor_hex.m
//  SerVik
//
//  Created by Kviatkovskii on 29.09.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "NSObject+KSColor_hex.h"

@implementation UIColor (KSColor_hex)

+ (UIColor *)colorWithHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1];
    [scanner scanHexInt:&rgbValue];
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
