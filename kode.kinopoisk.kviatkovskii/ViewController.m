//
//  ViewController.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 19.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    NSString *strCityID;
    NSString *strCityName;
    
    NSMutableDictionary *dictTitle;
    NSMutableDictionary *dictID;
    NSMutableDictionary *dictRaiting;
    NSMutableDictionary *dictPoster;
    NSMutableDictionary *dictCountry;
    NSMutableDictionary *dictGenre;
    NSMutableArray *arraySection;
    NSInteger offset;
    NSInteger sort;
    NSString *genre;
    NSDate *date;
}

@property (nonatomic, retain) UIPopoverPresentationController *calendarPopover;

@end

@implementation ViewController
/*
сортировка фильмов
 0 - высокий рейтинг
 1 - низкий рейтинг
 2 - без сортировки
*/

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [SVProgressHUD show];
    
    dictID = [NSMutableDictionary dictionary];
    dictTitle = [NSMutableDictionary dictionary];
    dictPoster = [NSMutableDictionary dictionary];
    dictRaiting = [NSMutableDictionary dictionary];
    dictCountry = [NSMutableDictionary dictionary];
    dictGenre = [NSMutableDictionary dictionary];
    arraySection = [NSMutableArray array];
    
    offset = 0;
    sort = 2;
    genre = @"ALL";
    date = [NSDate date];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"City"] != NULL) {
        NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"City"][@"cityName"]];
        [title addAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:20],
                               NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0, title.length)];
        [self.cityBtn setAttributedTitle:title forState:UIControlStateNormal];
        [self downloadFilmByCity:[[NSUserDefaults standardUserDefaults] objectForKey:@"City"][@"cityID"]
                        filmDate:[KSDate getNextDay:offset curDate:date]
                        filmSort:sort
                     genreFilter:genre];
    }
    else {
        [self performSegueWithIdentifier:@"citySegue" sender:self];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sortFilm:) name:@"SortFilm" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(selectDate:) name:@"CalendarDate" object:nil];
    
    self.footerView.hidden = TRUE;
    self.tableView.tableFooterView = self.footerView;
    
    [[NSUserDefaults standardUserDefaults] setValue:@(sort) forKey:@"CurSort"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"ListGenre"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    
    AppDelegate *shared = (AppDelegate*)[UIApplication sharedApplication].delegate;
    shared.blockRotation = TRUE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Select Date

- (void)selectDate:(NSNotification *)notification {
    [SVProgressHUD show];
    
    offset = 0;
    [self removeArrayAll];
    [self.tableView reloadData];
    
    if ([notification.object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *message = [notification object];
        if ([[notification name] isEqualToString:@"CalendarDate"]) {
            date = message[@"date"];
        }
    }
    
    [self downloadFilmByCity:[[NSUserDefaults standardUserDefaults] objectForKey:@"City"][@"cityID"]
                    filmDate:[KSDate getNextDay:offset curDate:date]
                    filmSort:sort
                 genreFilter:genre];
}

#pragma mark - Sort Film

- (void)sortFilm:(NSNotification *)notification {
    [SVProgressHUD show];
    
    offset = 0;
    [self removeArrayAll];
    [self.tableView reloadData];
    
    if ([notification.object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *message = [notification object];
        if ([[notification name] isEqualToString:@"SortFilm"]) {
            sort = [message[@"sort"] integerValue];
        }
    }
    
    [self downloadFilmByCity:[[NSUserDefaults standardUserDefaults] objectForKey:@"City"][@"cityID"]
                    filmDate:[KSDate getNextDay:offset curDate:date]
                    filmSort:sort
                    genreFilter:genre];
}

#pragma mark - Filter Delegate

- (void)listGenreName:(NSMutableArray *)arrayName {
    [SVProgressHUD show];
    
    offset = 0;
    [self removeArrayAll];
    [self.tableView reloadData];
    
    NSMutableString *strName = [NSMutableString string];
    if (arrayName != nil) {
        [arrayName enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [strName appendFormat:@",%@", obj];
        }];
        genre = [strName substringFromIndex:1];
    }
    else {
        genre = @"ALL";
    }
    
    [self downloadFilmByCity:[[NSUserDefaults standardUserDefaults] objectForKey:@"City"][@"cityID"]
                    filmDate:[KSDate getNextDay:offset curDate:date]
                    filmSort:sort
                 genreFilter:genre];
}

#pragma mark - City Delegate

- (void)getCity:(NSString *)cityID nameCity:(NSString *)name {
    strCityID = cityID;
    strCityName = name;
}

- (void)saveCity {
    [SVProgressHUD show];
    
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:strCityName];
    [title addAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:20],
                           NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0, title.length)];
    [self.cityBtn setAttributedTitle:title forState:UIControlStateNormal];
    
    offset = 0;
    sort = 2;
    genre = @"ALL";
    
    [self removeArrayAll];
    [self.tableView reloadData];
    
    [[NSUserDefaults standardUserDefaults] setObject:@{@"cityID":strCityID, @"cityName":strCityName} forKey:@"City"];
    [[NSUserDefaults standardUserDefaults] setValue:@(sort) forKey:@"CurSort"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"ListGenre"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self downloadFilmByCity:strCityID filmDate:[KSDate getNextDay:offset curDate:date] filmSort:sort genreFilter:genre];
}

#pragma mark - Download Film

- (void)downloadFilmByCity:(NSString *)cityID filmDate:(NSString *)Date filmSort:(NSInteger)parameter genreFilter:(NSString *)genreName {
    __weak typeof(self) weakSelf = self;
    
    [KSLoader downloadFilmByCity:cityID filmDate:Date filmSort:parameter genreFilter:genreName block:^(NSDictionary *completion) {
        //NSLog(@"%@", completion);
        if ([completion[@"isEmpty"] isEqual:@0]) {
            [weakSelf.tableView beginUpdates];
            
            [dictGenre addEntriesFromDictionary:completion[@"Genre"]];
            [dictTitle addEntriesFromDictionary:completion[@"Title"]];
            [dictCountry addEntriesFromDictionary:completion[@"Country"]];
            [dictRaiting addEntriesFromDictionary:completion[@"Raiting"]];
            [dictPoster addEntriesFromDictionary:completion[@"Poster"]];
            [dictID addEntriesFromDictionary:completion[@"ID"]];

            [weakSelf.tableView insertSections:[NSIndexSet indexSetWithIndex:offset] withRowAnimation:UITableViewRowAnimationFade];
            [arraySection insertObject:completion[@"Date"] atIndex:offset];

            [weakSelf.tableView endUpdates];
            
            weakSelf.footerView.hidden = FALSE;
            [weakSelf.indicatorActivity stopAnimating];
            weakSelf.indicatorActivity.hidden = TRUE;
            weakSelf.loadMoreBtn.hidden = FALSE;
        } else {
            offset--;
            weakSelf.footerView.hidden = TRUE;
        }
    }];
}

#pragma mark - Button Action

- (IBAction)pressCalendarBtn:(UIButton *)sender {
    KSHeaderMainViewCollectionViewController *calendarView = [self.storyboard instantiateViewControllerWithIdentifier:@"calendarView"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        UINavigationController *destNav = [[UINavigationController alloc] initWithRootViewController:calendarView];
        
        calendarView.preferredContentSize = CGSizeMake(self.view.frame.size.width, 255);
        destNav.modalPresentationStyle = UIModalPresentationPopover;
        _calendarPopover = destNav.popoverPresentationController;
        _calendarPopover.delegate = self;
        _calendarPopover.sourceView = self.view;
        _calendarPopover.sourceRect = sender.frame;
        destNav.navigationBarHidden = TRUE;
        [self presentViewController:destNav animated:TRUE completion:nil];
    } else {
        UIPopoverController *userDataPopover = [[UIPopoverController alloc] initWithContentViewController:calendarView];
        userDataPopover.popoverContentSize = CGSizeMake(350, 255);
        [userDataPopover presentPopoverFromRect:sender.frame
                                         inView:self.view
                       permittedArrowDirections:UIPopoverArrowDirectionUp
                                       animated:YES];
    }
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arraySection.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return arraySection[section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *sectionTitle = arraySection[section];
    NSArray *array = dictID[sectionTitle];
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *sectionTitle = arraySection[indexPath.section];
    NSArray *array = dictID[sectionTitle];
    
    cell.titleMainCell.text = dictTitle[array[indexPath.row]];
    cell.genreMainCell.text = dictGenre[array[indexPath.row]];
    [cell.imageMainCell sd_setImageWithURL:dictPoster[array[indexPath.row]] placeholderImage:nil options:SDWebImageHighPriority];
    cell.raitingMainCell.text = dictRaiting[array[indexPath.row]];
    cell.countryMainCell.text = dictCountry[array[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger lastSectionIndex = tableView.numberOfSections - 1;
    NSString *sectionTitle = arraySection[lastSectionIndex];
    NSArray *array = dictID[sectionTitle];
    
    NSInteger lastRowIndex = array.count > 1 ? [tableView numberOfRowsInSection:lastSectionIndex] - 2 : [tableView numberOfRowsInSection:lastSectionIndex] - 1;
    
    if ((indexPath.section == lastSectionIndex) && (indexPath.row == lastRowIndex)) {
        [self pressLoadMoreBtn:self];
    }
}

#pragma mark - Button Action

- (IBAction)pressLoadMoreBtn:(id)sender {
    self.loadMoreBtn.hidden = TRUE;
    self.indicatorActivity.hidden = FALSE;
    [self.indicatorActivity startAnimating];
    
    offset++;
    [self downloadFilmByCity:[[NSUserDefaults standardUserDefaults] objectForKey:@"City"][@"cityID"]
                    filmDate:[KSDate getNextDay:offset curDate:date]
                    filmSort:sort
                 genreFilter:genre];
}

#pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"citySegue"]) {
         KSMenuCityViewController *destination = (KSMenuCityViewController *)[segue.destinationViewController topViewController];
         destination.delegate = self;
    }
    else if ([segue.identifier isEqualToString:@"headerMenu"]) {
        KSHeaderMainViewCollectionViewController *destination = segue.destinationViewController;
        destination.delegate = self;
    }
    else if ([segue.identifier isEqualToString:@"detailFilmSegue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        NSString *sectionTitle = arraySection[indexPath.section];
        NSArray *array = dictID[sectionTitle];

        KSDetailFilmViewController *destination = segue.destinationViewController;
        destination.IDFilm = array[indexPath.row];
        destination.offsetDay = [KSDate convertStringDateToYandexDate:sectionTitle];
        destination.titleNavBar = sectionTitle;
        destination.ratingKinopoisk = dictRaiting[array[indexPath.row]];
    }
}

- (void)removeArrayAll {
    [arraySection removeAllObjects];
    [dictID removeAllObjects];
    [dictTitle removeAllObjects];
    [dictCountry removeAllObjects];
    [dictRaiting removeAllObjects];
    [dictPoster removeAllObjects];
}

@end
