//
//  KSRatingFilmViewController.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 06.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KSCollectionViewCell.h"
#import "KSLoader.h"
#import "KSLoaderIMDB.h"

@interface KSRatingFilmViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSString *rateKinopoisk;
@property (strong, nonatomic) NSString *filmID;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end
