//
//  KSMapViewController.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 29.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface KSMapViewController : UIViewController <MKMapViewDelegate>

@property (strong, nonatomic) NSString *titleCinema;
@property (strong, nonatomic) NSString *addressCinema;
@property (strong, nonatomic) NSDictionary *dictCoordinates;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end
