//
//  KSFilterFilmTableViewController.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 19.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSFilterFilmTableViewController.h"

@interface KSFilterFilmTableViewController () {
    NSMutableArray *arrayID;
    NSMutableArray *arrayName;
    NSMutableArray *selectName;
    NSMutableIndexSet *indexSet;
}

@end

@implementation KSFilterFilmTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrayID = [NSMutableArray array];
    arrayName = [NSMutableArray array];
    selectName = [NSMutableArray array];
    
    if ([[NSUserDefaults standardUserDefaults] dataForKey:@"ListGenre"] != nil) {
        NSData *data = [[NSUserDefaults standardUserDefaults] dataForKey:@"ListGenre"];
        indexSet = [[NSMutableIndexSet alloc] initWithIndexSet:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    } else {
        indexSet = [NSMutableIndexSet indexSet];
    }
    

    [self downloadGenreList];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Download Genre

- (void)downloadGenreList {
    __weak typeof(self) weakSelf = self;
    
    [KSLoader downloadFilter:^(NSDictionary *completion) {
        arrayName = completion[@"Name"];
        arrayID = completion[@"ID"];
        
        if (indexSet.count > 0) {
            selectName = [NSMutableArray arrayWithArray:[arrayName objectsAtIndexes:indexSet]];
        }
        [weakSelf.tableView reloadData];
    }];
}

#pragma mark - Button Action

- (IBAction)pressCloseBtn:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)pressApplyBtn:(id)sender {
    if (indexSet.count != 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ListGenreNotification" object:@{@"listGenreName":selectName,
                                                                                                     @"useFilter":@1}];
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:indexSet];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"ListGenre"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ListGenreNotification" object:@{@"listGenreName":@[],
                                                                                                     @"useFilter":@0}];
        [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"ListGenre"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayName.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.textLabel.text = arrayName[indexPath.row];
    
    if ([indexSet containsIndex:indexPath.row]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];

    if (![indexSet containsIndex:indexPath.row]) {
        [indexSet addIndex:indexPath.row];
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
        
        [selectName addObject:arrayName[indexPath.row]];
    }
    else {
        [indexSet removeIndex:indexPath.row];
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        
        [selectName removeObject:arrayName[indexPath.row]];
    }
}

#pragma mark - Coding Delegate

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self != nil) {
        indexSet = [aDecoder decodeObjectForKey:@"IndexSet"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:indexSet forKey:@"IndexSet"];
}

@end
