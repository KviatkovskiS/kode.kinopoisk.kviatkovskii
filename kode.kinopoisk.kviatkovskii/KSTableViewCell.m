//
//  KSTableViewCell.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 19.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSTableViewCell.h"

@implementation KSTableViewCell

- (void)drawRect:(CGRect)rect {
    [self cornerRadiusView:self.imageMainCell cornerWidth:5.f cornerHeight:5.f];
    [self cornerRadiusView:self.backgroundMainCell cornerWidth:5.f cornerHeight:5.f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)cornerRadiusView:(UIView *)view cornerWidth:(float)width cornerHeight:(float)height {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                   byRoundingCorners:UIRectCornerAllCorners
                                                         cornerRadii:CGSizeMake(width, height)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
    view.layer.masksToBounds = TRUE;
}

@end
