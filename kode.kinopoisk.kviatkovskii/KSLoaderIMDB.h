//
//  KSLoaderIMDB.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 05.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface KSLoaderIMDB : NSObject

+ (void)downloadIMDBShortData:(NSString *)ttIMDB block:(void(^)(NSDictionary *completion))completion;

@end
