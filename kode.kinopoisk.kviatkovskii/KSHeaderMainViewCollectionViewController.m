//
//  KSHeaderMainViewCollectionViewController.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 19.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSHeaderMainViewCollectionViewController.h"

@interface KSHeaderMainViewCollectionViewController () {
    NSArray *array;
    NSArray *arrayImage;
}

@property (nonatomic, retain) UIPopoverPresentationController *sortPopover;

@end

@implementation KSHeaderMainViewCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    array = @[@"Сортировка", @"Фильтр"];
    arrayImage = @[[UIImage imageNamed:@"sort"], [UIImage imageNamed:@"filter"]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getListGenre:) name:@"ListGenreNotification" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Filter Notification

- (void)getListGenre:(NSNotification *)notification {
    if ([notification.object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *message = [notification object];
        if ([[notification name] isEqualToString:@"ListGenreNotification"]) {
            if ([message[@"useFilter"] integerValue] == 1) {
                [self.delegate listGenreName:message[@"listGenreName"]];
            }
            else {
                [self.delegate listGenreName:nil];
            }
        }
    }
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    KSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    cell.titleHeaderCell.text = array[indexPath.row];
    cell.imageHeaderCell.image = arrayImage[indexPath.row];
    
    if (indexPath.row == 0) {
        cell.titleHeaderCell.textAlignment = NSTextAlignmentLeft;
    }
    else {
        cell.imageHeaderCell.transform = CGAffineTransformMakeTranslation(cell.frame.size.width - 35, 0);
        cell.titleHeaderCell.transform = CGAffineTransformMakeTranslation(-40, 0);
        cell.titleHeaderCell.textAlignment = NSTextAlignmentRight;
    }
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if (result.height <= 568) {
            return CGSizeMake(152, 35);
        }
        if (result.height == 667) {
            return CGSizeMake(180, 35);
        }
        if (result.height == 736) {
            return CGSizeMake(195, 35);
        }
    }
    else {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if (result.height == 1366 || result.width == 1366) {
            return CGSizeMake(182, 55);
        } else {
            return CGSizeMake(152, 55);
        }
    }
    return CGSizeMake(0, 0);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        return UIEdgeInsetsMake(0, 5, 0, 5);
    }
    else {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    KSCollectionViewCell *cell = (KSCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if (indexPath.row == 0) {
        [self createPopoverSortView:cell];
    }
    else {
        KSHeaderMainViewCollectionViewController *filterView = [self.storyboard instantiateViewControllerWithIdentifier:@"filterView"];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self presentViewController:filterView animated:TRUE completion:nil];
        }
        else {
            UIPopoverController *userDataPopover = [[UIPopoverController alloc] initWithContentViewController:filterView];
            userDataPopover.popoverContentSize = CGSizeMake(350, 600);
            [userDataPopover presentPopoverFromRect:cell.frame
                                             inView:self.view
                           permittedArrowDirections:UIPopoverArrowDirectionUp
                                           animated:YES];
        }
    }
}

#pragma mark - Popover Sort View

- (void)createPopoverSortView:(KSCollectionViewCell *)cell {
    KSHeaderMainViewCollectionViewController *sortView = [self.storyboard instantiateViewControllerWithIdentifier:@"sortView"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        UINavigationController *destNav = [[UINavigationController alloc] initWithRootViewController:sortView];
        
        sortView.preferredContentSize = CGSizeMake(self.view.frame.size.width, 45);
        destNav.modalPresentationStyle = UIModalPresentationPopover;
        _sortPopover = destNav.popoverPresentationController;
        _sortPopover.delegate = self;
        _sortPopover.sourceView = self.view;
        
        _sortPopover.sourceRect = cell.frame;
        destNav.navigationBarHidden = TRUE;
        [self presentViewController:destNav animated:TRUE completion:nil];
    }
    else {
        UIPopoverController *userDataPopover = [[UIPopoverController alloc] initWithContentViewController:sortView];
        userDataPopover.popoverContentSize = CGSizeMake(350, 90);
        [userDataPopover presentPopoverFromRect:cell.frame
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionUp
                                            animated:YES];
    }
}

- (UIModalPresentationStyle)adaptivePresentationStyleForPresentationController:(UIPresentationController *)controller {
    return UIModalPresentationNone;
}

@end
