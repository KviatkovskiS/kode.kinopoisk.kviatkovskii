//
//  KSSortFilmTableViewController.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 19.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSSortFilmTableViewController.h"

@interface KSSortFilmTableViewController () {
    NSArray *array;
    NSIndexPath *oldIndexPath;
}

@end

@implementation KSSortFilmTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    array = @[@"Высокий рейтинг", @"Низкий рейтинг"];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.titleSortCell.text = array[indexPath.row];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"CurSort"] integerValue] == indexPath.row) {
        cell.imageSortCell.hidden = FALSE;
        oldIndexPath = indexPath;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
    KSTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (oldIndexPath == nil) {
        oldIndexPath = indexPath;
        cell.imageSortCell.hidden = FALSE;
    }
    else {
        KSTableViewCell *selectCell = [tableView cellForRowAtIndexPath:oldIndexPath];
        selectCell.imageSortCell.hidden = TRUE;
        
        cell.imageSortCell.hidden = FALSE;
        oldIndexPath = indexPath;
    }
    [[NSUserDefaults standardUserDefaults] setValue:@(oldIndexPath.row) forKey:@"CurSort"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SortFilm" object:@{@"sort":@(oldIndexPath.row)}];
    
    //[self dismissViewControllerAnimated:TRUE completion:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.f;
}

@end
