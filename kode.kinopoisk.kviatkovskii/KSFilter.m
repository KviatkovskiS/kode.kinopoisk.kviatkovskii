//
//  KSFilter.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 27.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSFilter.h"

@implementation KSFilter

+ (BOOL)containsArray:(NSArray *)array genreString:(NSString *)genre {
    for (NSString *curGenre in array) {
        if ([genre containsString:curGenre]) {
            return TRUE;
        }
    }
    return FALSE;
}

+ (id)addNameCityInDict:(id)dict valueDict:(id)value keyDict:(id)key {
    if (dict[key] != nil) {
        id object = dict[key];
        [object addObject:value];
        [dict setValue:object forKey:key];
    }
    else {
        NSMutableArray *object = [NSMutableArray arrayWithObject:value];
        [dict setValue:object forKey:key];
    }
    return dict;
}

@end
