//
//  KSCalenderViewController.h
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 09.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FSCalendar/FSCalendar.h>

@interface KSCalenderViewController : UIViewController <FSCalendarDelegate, FSCalendarDataSource>

@property (weak, nonatomic) IBOutlet FSCalendar *calendarView;

@end
