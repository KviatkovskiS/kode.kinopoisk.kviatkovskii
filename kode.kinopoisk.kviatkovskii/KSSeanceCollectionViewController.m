//
//  KSSeanceCollectionViewController.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 28.10.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSSeanceCollectionViewController.h"

@interface KSSeanceCollectionViewController () {
    NSArray *sectionArray;
    NSMutableDictionary *dictTime;
    NSMutableDictionary *dictTime3D;
    NSMutableDictionary *dictTime2D;
    NSMutableArray *arrayLocation;
    NSMutableArray *arrayAddress;
    NSInteger selectSection;
    NSString *strDate;
}

@end

@implementation KSSeanceCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];

    dictTime = [NSMutableDictionary dictionary];
    dictTime3D = [NSMutableDictionary dictionary];
    dictTime2D = [NSMutableDictionary dictionary];
    arrayLocation = [NSMutableArray array];
    arrayAddress = [NSMutableArray array];
    
    NSDictionary *dictParam = [[NSUserDefaults standardUserDefaults] objectForKey:@"SeanceFilm"];
    NSDictionary *dictCity = [[NSUserDefaults standardUserDefaults] objectForKey:@"City"];
    [self downloadSeanseFilm:dictParam[@"filmID"]
                  filmInCity:dictCity[@"cityID"]
                  filmInDate:dictParam[@"offsetDay"]];
    strDate = dictParam[@"Date"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Download Seanse

- (void)downloadSeanseFilm:(NSString *)filmID filmInCity:(NSString *)cityID filmInDate:(NSString *)date {
    __weak typeof(self) weakSelf = self;
    
    [KSLoader downloadSeanseFilm:filmID filmInCity:cityID filmInDate:date block:^(NSDictionary *completion) {
        weakSelf.navigationItem.title = completion[@"Title"];
        arrayAddress = completion[@"Address"];
        arrayLocation = completion[@"Location"];
        dictTime = completion[@"Seanse"];
        dictTime2D = completion[@"Seanse2D"];
        dictTime3D = completion[@"Seanse3D"];
        sectionArray = [dictTime allKeys];
        
        [weakSelf.collectionView reloadData];
    }];
}

#pragma mark - Collection View

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return sectionArray.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSString *sectionTitle = sectionArray[section];
    NSArray *array = dictTime[sectionTitle];
    return array.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reusableView = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        KSCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                        withReuseIdentifier:@"headerView"
                                                               forIndexPath:indexPath];
        
        headerView.titleHeaderCell.text = sectionArray[indexPath.section];
        headerView.addressHeaderCell.text = arrayAddress[indexPath.section];
        [headerView.btnHeaderCell addTarget:self action:@selector(pressSection:) forControlEvents:UIControlEventTouchDown];
        headerView.btnHeaderCell.tag = indexPath.section;
        reusableView = headerView;
    }
    return reusableView;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    KSCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    NSString *sectionTitle = sectionArray[indexPath.section];
    NSArray *array = dictTime[sectionTitle];
    
    cell.titleSeanceCell.text = array[indexPath.row];
    
    if (dictTime3D[sectionTitle]) {
        if ([dictTime3D[sectionTitle] containsObject:array[indexPath.row]]) {
            cell.lbl3dSeanceCell.hidden = FALSE;
        } else {
            cell.lbl3dSeanceCell.hidden = TRUE;
        }
    }
    
    if (dictTime2D[sectionTitle]) {
        if ([dictTime2D[sectionTitle] containsObject:array[indexPath.row]]) {
            cell.lbl2dSeanceCell.hidden = FALSE;
        } else {
            cell.lbl2dSeanceCell.hidden = TRUE;
        }
    }
    
    if ([KSDate nowTimeCompareSeanceTime:[NSString stringWithFormat:@"%@ %@", strDate, array[indexPath.row]]]) {
        cell.alpha = 1.f;
    } else {
        cell.alpha = .4f;
    }
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if (result.height <= 568) {
            return CGSizeMake(90, 50);
        }
        if (result.height == 667) {
            return CGSizeMake(100, 50);
        }
        if (result.height == 736) {
            return CGSizeMake(100, 50);
        }
    }
    else {
        return CGSizeMake(130, 70);
    }
    return CGSizeMake(0, 0);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if (result.height <= 568) {
            return UIEdgeInsetsMake(10, 10, 10, 10);
        }
        if (result.height == 667) {
            return UIEdgeInsetsMake(10, 20, 10, 20);
        }
        if (result.height == 736) {
            return UIEdgeInsetsMake(10, 30, 10, 30);
        }
    }
    else {
        return UIEdgeInsetsMake(10, 10, 10, 10);;
    }
    return UIEdgeInsetsMake(20, 20, 20, 20);
}

- (void)pressSection:(UIButton *)sender {
    selectSection = sender.tag;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"mapSegue"]) {
        KSMapViewController *destination = (KSMapViewController *)[segue.destinationViewController topViewController];
        destination.titleCinema = sectionArray[selectSection];
        destination.addressCinema = arrayAddress[selectSection];
        destination.dictCoordinates = arrayLocation[selectSection];
    }
}

@end
