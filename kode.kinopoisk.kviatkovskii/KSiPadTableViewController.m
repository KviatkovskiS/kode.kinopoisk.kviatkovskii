//
//  KSiPadTableViewController.m
//  kode.kinopoisk.kviatkovskii
//
//  Created by Kviatkovskii on 08.11.16.
//  Copyright © 2016 Sergei Kviatkovskii. All rights reserved.
//

#import "KSiPadTableViewController.h"

@interface KSiPadTableViewController () {
    NSString *strCityID;
    NSString *strCityName;
    
    NSMutableDictionary *dictTitle;
    NSMutableDictionary *dictID;
    NSMutableDictionary *dictRaiting;
    NSMutableDictionary *dictPoster;
    NSMutableDictionary *dictCountry;
    NSMutableDictionary *dictGenre;
    NSMutableArray *arraySection;
    NSInteger offset;
    NSInteger sort;
    NSString *genre;
    NSDate *date;
    BOOL firstLaunch;
}

@end

@implementation KSiPadTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[SVProgressHUD show];
    dictID = [NSMutableDictionary dictionary];
    dictTitle = [NSMutableDictionary dictionary];
    dictPoster = [NSMutableDictionary dictionary];
    dictRaiting = [NSMutableDictionary dictionary];
    dictCountry = [NSMutableDictionary dictionary];
    dictGenre = [NSMutableDictionary dictionary];
    arraySection = [NSMutableArray array];
    
    offset = 0;
    sort = 2;
    genre = @"ALL";
    date = [NSDate date];
    firstLaunch = TRUE;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"City"] != NULL) {
        NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"City"][@"cityName"]];
        [title addAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:20],
                               NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0, title.length)];
        [self.cityBtn setAttributedTitle:title forState:UIControlStateNormal];
        [self downloadFilmByCity:[[NSUserDefaults standardUserDefaults] objectForKey:@"City"][@"cityID"]
                        filmDate:[KSDate getNextDay:offset curDate:date]
                        filmSort:sort
                     genreFilter:genre];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sortFilm:) name:@"SortFilm" object:nil];
    
    self.footerView.hidden = TRUE;
    self.tableView.tableFooterView = self.footerView;
    
    self.headerView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 55);
    self.tableView.tableHeaderView = self.headerView;
    
    [[NSUserDefaults standardUserDefaults] setValue:@(sort) forKey:@"CurSort"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"ListGenre"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)viewDidLayoutSubviews {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"City"] == NULL) {
        [self performSegueWithIdentifier:@"citySegue" sender:self];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Sort Film

- (void)sortFilm:(NSNotification *)notification {
    [SVProgressHUD show];
    
    offset = 0;
    [self removeArrayAll];
    [self.tableView reloadData];
    
    if ([notification.object isKindOfClass:[NSDictionary class]]) {
        NSDictionary *message = [notification object];
        if ([[notification name] isEqualToString:@"SortFilm"]) {
            sort = [message[@"sort"] integerValue];
        }
    }
    
    [self downloadFilmByCity:[[NSUserDefaults standardUserDefaults] objectForKey:@"City"][@"cityID"]
                    filmDate:[KSDate getNextDay:offset curDate:date]
                    filmSort:sort
                 genreFilter:genre];
}

#pragma mark - Filter Delegate

- (void)listGenreName:(NSMutableArray *)arrayName {
    [SVProgressHUD show];
    
    offset = 0;
    [self removeArrayAll];
    [self.tableView reloadData];
    
    NSMutableString *strName = [NSMutableString string];
    if (arrayName != nil) {
        [arrayName enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [strName appendFormat:@",%@", obj];
        }];
        genre = [strName substringFromIndex:1];
    }
    else {
        genre = @"ALL";
    }
    
    [self downloadFilmByCity:[[NSUserDefaults standardUserDefaults] objectForKey:@"City"][@"cityID"]
                    filmDate:[KSDate getNextDay:offset curDate:date]
                    filmSort:sort
                 genreFilter:genre];
}

#pragma mark - City Delegate

- (void)getCity:(NSString *)cityID nameCity:(NSString *)name {
    strCityID = cityID;
    strCityName = name;
}

- (void)saveCity {
    [SVProgressHUD show];
    
    NSMutableAttributedString *title = [[NSMutableAttributedString alloc] initWithString:strCityName];
    [title addAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:20],
                           NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0, title.length)];
    [self.cityBtn setAttributedTitle:title forState:UIControlStateNormal];
    
    offset = 0;
    sort = 2;
    genre = @"ALL";
    
    [self removeArrayAll];
    [self.tableView reloadData];
    
    [[NSUserDefaults standardUserDefaults] setObject:@{@"cityID":strCityID, @"cityName":strCityName} forKey:@"City"];
    [[NSUserDefaults standardUserDefaults] setValue:@(sort) forKey:@"CurSort"];
    [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"ListGenre"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self downloadFilmByCity:strCityID filmDate:[KSDate getNextDay:offset curDate:date] filmSort:sort genreFilter:genre];
}

#pragma mark - Button Action

- (IBAction)pressLoadMoreBtn:(id)sender {
    self.loadMoreBtn.hidden = TRUE;
    self.indicatorActivity.hidden = FALSE;
    [self.indicatorActivity startAnimating];
    
    offset++;
    [self downloadFilmByCity:[[NSUserDefaults standardUserDefaults] objectForKey:@"City"][@"cityID"]
                    filmDate:[KSDate getNextDay:offset curDate:date]
                    filmSort:sort
                 genreFilter:genre];
}

#pragma mark - Download Film

- (void)downloadFilmByCity:(NSString *)cityID filmDate:(NSString *)Date filmSort:(NSInteger)parameter genreFilter:(NSString *)genreName {
    __weak typeof(self) weakSelf = self;
    
    [KSLoader downloadFilmByCity:cityID filmDate:Date filmSort:parameter genreFilter:genreName block:^(NSDictionary *completion) {
        //NSLog(@"%@", completion);
        if ([completion[@"isEmpty"] isEqual:@0]) {
            [weakSelf.tableView beginUpdates];
            
            [dictGenre addEntriesFromDictionary:completion[@"Genre"]];
            [dictTitle addEntriesFromDictionary:completion[@"Title"]];
            [dictCountry addEntriesFromDictionary:completion[@"Country"]];
            [dictRaiting addEntriesFromDictionary:completion[@"Raiting"]];
            [dictPoster addEntriesFromDictionary:completion[@"Poster"]];
            [dictID addEntriesFromDictionary:completion[@"ID"]];
            
            [weakSelf.tableView insertSections:[NSIndexSet indexSetWithIndex:offset] withRowAnimation:UITableViewRowAnimationFade];
            [arraySection insertObject:completion[@"Date"] atIndex:offset];
            
            [weakSelf.tableView endUpdates];
            
            if (firstLaunch) {
                firstLaunch = FALSE;
                [weakSelf.tableView.delegate tableView:weakSelf.tableView didSelectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                [weakSelf performSegueWithIdentifier:@"detailFilmSegue" sender:weakSelf];
            }
            
            weakSelf.footerView.hidden = FALSE;
            [weakSelf.indicatorActivity stopAnimating];
            weakSelf.indicatorActivity.hidden = TRUE;
            weakSelf.loadMoreBtn.hidden = FALSE;
        }
        else {
            offset--;
            weakSelf.footerView.hidden = TRUE;
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arraySection.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return arraySection[section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *sectionTitle = arraySection[section];
    NSArray *array = dictID[sectionTitle];
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    KSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *sectionTitle = arraySection[indexPath.section];
    NSArray *array = dictID[sectionTitle];
    
    cell.titleMainCell.text = dictTitle[array[indexPath.row]];
    cell.genreMainCell.text = dictGenre[array[indexPath.row]];
    [cell.imageMainCell sd_setImageWithURL:dictPoster[array[indexPath.row]] placeholderImage:nil options:SDWebImageHighPriority];
    cell.raitingMainCell.text = dictRaiting[array[indexPath.row]];
    cell.countryMainCell.text = dictCountry[array[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:TRUE];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger lastSectionIndex = tableView.numberOfSections - 1;
    NSString *sectionTitle = arraySection[lastSectionIndex];
    NSArray *array = dictID[sectionTitle];
    
    NSInteger lastRowIndex = array.count > 1 ? [tableView numberOfRowsInSection:lastSectionIndex] - 2 : [tableView numberOfRowsInSection:lastSectionIndex] - 1;
    
    if ((indexPath.section == lastSectionIndex) && (indexPath.row == lastRowIndex)) {
        [self pressLoadMoreBtn:self];
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"citySegue"]) {
        KSMenuCityViewController *destination = (KSMenuCityViewController *)[segue.destinationViewController topViewController];
        destination.delegate = self;
    }
    else if ([segue.identifier isEqualToString:@"headerMenu"]) {
        KSHeaderMainViewCollectionViewController *destination = segue.destinationViewController;
        destination.delegate = self;
    }
    else if ([segue.identifier isEqualToString:@"detailFilmSegue"]) {
        
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        NSString *sectionTitle = arraySection[indexPath.section];
        NSArray *array = dictID[sectionTitle];
        
        KSDetailFilmViewController *destination = (KSDetailFilmViewController *)[segue.destinationViewController topViewController];
        destination.IDFilm = array[indexPath.row];
        destination.offsetDay = [KSDate convertStringDateToYandexDate:sectionTitle];
        destination.titleNavBar = sectionTitle;
        destination.ratingKinopoisk = dictRaiting[array[indexPath.row]];
    }
}

- (void)removeArrayAll {
    [arraySection removeAllObjects];
    [dictID removeAllObjects];
    [dictTitle removeAllObjects];
    [dictCountry removeAllObjects];
    [dictRaiting removeAllObjects];
    [dictPoster removeAllObjects];
}
@end
