```
#objective-c #iOS 8 #Xcode 8


```
### **ВПрокате** ###

ВПрокате покажет вам какие фильмы собираются показывать кинотеатры вашего города.

Ключевые особенности:
* Расписание для кинотеатров вашего города.
* Сортировка фильмов по рейтингу.
* Настройка фильтров по жанрам.
* Выбор любого города России.
* Описание для каждого фильма.
* Трейлеры и кадры из фильмов.

Первоначально проект выполнятся как тестовое задание, но потом перерос в целое приложение, которое готовилось к релизу. Но в последний момент [апи](http://kinopoisk.cf) закрыли и приложение так и не вышло в свет. Решил выложить исходный код.

# Скриншот iPhone #
![0x0ss.jpg](https://bitbucket.org/repo/RELGor/images/2928612214-0x0ss.jpg)

# Скриншот iPad #
![0x0ss-6.jpg](https://bitbucket.org/repo/RELGor/images/2484930295-0x0ss-6.jpg)

# Библиотеки #
- [AFNetworking](https://github.com/AFNetworking/AFNetworking)
- [SDWebImage](https://github.com/rs/SDWebImage)
- [IDMPhotoBrowser](https://github.com/ideaismobile/IDMPhotoBrowser)
- [SVProgressHUD](https://github.com/SVProgressHUD/SVProgressHUD)
- [FSCalendar](https://github.com/WenchaoD/FSCalendar)